<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\grid;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * Description of LabelColumn
 *
 * @author Tartharia
 */
class LabelColumn extends \yii\grid\DataColumn{

    public $labelTypes = [];

    public $hasMaster = false;

    protected function renderDataCellContent($model, $key, $index) {
        $this->format = 'html';
        $attribute = $this->hasMaster?$model->masterAttribute($this->attribute):$this->attribute;
        $labelType = ArrayHelper::getValue($this->labelTypes, $model->{$attribute}, 'primary');
        return Html::tag('span',parent::renderDataCellContent($model, $key, $index),['class'=>"label label-{$labelType}"]);
    }
}
