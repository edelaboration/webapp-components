<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\grid;

use yii\helpers\ArrayHelper;

/**
 * Description of ArrayColumn
 *
 * @author Tartharia
 */
class ArrayColumn extends \yii\grid\DataColumn{

    public $listOptions=[];

    public function getDataCellValue($model, $key, $index)
    {
        $value = [];

        if(($pos = strpos($this->attribute, '.')) !== false)
        {
            $array = ArrayHelper::getValue($model, substr($this->attribute, 0, $pos));
            $param= substr($this->attribute, $pos + 1);
        }
        if(isset($array) && is_array($array))
        {
            foreach ($array as $item) {
                $value[]=  ArrayHelper::getValue($item, $param);
            }
        }  else {
            $value = parent::getDataCellValue($model, $key, $index);
        }

        return $value;
    }

    protected function renderDataCellContent($model, $key, $index) {
        if ($this->content === null) {
            $value = $this->getDataCellValue($model, $key, $index);
            if(is_array($value))
            {
                $this->format='html';
                $value = '<ul'.\yii\helpers\Html::renderTagAttributes($this->listOptions).'><li>'.implode('</li><li>', $value).'</li></ul>';
            }
            return $this->grid->formatter->format($value, $this->format);
        } else {
            return parent::renderDataCellContent($model, $key, $index);
        }
    }
}
