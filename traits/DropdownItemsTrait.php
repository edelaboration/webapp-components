<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\traits;

use Yii;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;

/**
 *
 * @author Tartharia
 */
trait DropdownItemsTrait
{
    public static function dropdownItems($from,$to)
    {
        if(!($items = Yii::$app->cache->get(static::getCacheKey())))
        {
            $items = ArrayHelper::map(static::find()->asArray()->all(),$from,$to);
            Yii::$app->cache->set(static::getCacheKey(), $items,null,new TagDependency([
                'tags'=> static::getDependencyTag()
            ]));
        }
        return $items;
    }

    /**
     *
     * @param string $label
     * @param string $route
     * @param array $params
     * @return array
     */
    public static function navItems($label,$route,$params=[])
    {
        if(!($items = Yii::$app->cache->get(static::getCacheKey('menu'))))
        {
            $items= static::getItems($label, $route, $params);
            Yii::$app->cache->set(static::getCacheKey('menu'), $items,null,new TagDependency([
                'tags'=> static::getDependencyTag()
            ]));
        }
        return $items;
    }

    /**
     *
     * @param string $label
     * @param string $route
     * @param array $params
     * @param \Closure $countCallback
     * @param string $layout
     * @return array
     */
    public static function getItems($label,$route,$params=[],$countCallback=null,$layout="{label}")
    {
        $items=[];
        $owner = \Yii::createObject(static::className());
        $select = ArrayHelper::merge((array)"PT.$label",
            array_map(function($item){
                return "PT.{$item}";
            }, array_intersect($params, $owner->attributes()))
        );
        $query = static::find()->alias('PT')->select(implode(',', $select))->asArray();
        if($countCallback instanceof \Closure)
        {
            $query = call_user_func($countCallback,$query);
        }
        $rows = $query->all();

        foreach ($rows as $row)
        {
            $item = [
                'label'=> Yii::$app->i18n->format($layout,['label'=>$row[$label],'quantity'=> ArrayHelper::getValue($row,'quantity')], Yii::$app->language),
                'url'=>[$route]
            ];
            unset ($row[$label]);
            foreach ($params as $param => $attribute)
            {
                if(!empty($attribute))
                {
                    $item['url'][$param]= ArrayHelper::getValue($row, $attribute, $attribute);//$row[$attribute];
                }
            }
            $items[]=$item;
        }
        return $items;
    }

    public function invalidateItems()
    {
        TagDependency::invalidate(Yii::$app->cache, static::getDependencyTag());
    }

    public function attachInvalidate()
    {
        $this->on(ActiveRecord::EVENT_AFTER_UPDATE,[$this,'invalidateItems']);
        $this->on(ActiveRecord::EVENT_AFTER_INSERT,[$this,'invalidateItems']);
        $this->on(ActiveRecord::EVENT_AFTER_DELETE,[$this,'invalidateItems']);
    }

    public final static function getCacheKey($type = 'dropdown')
    {
        $key = parent::className();
        $pos = strrpos($key,'\\');
        return $type . 'Robote13Catalog' . substr($key, $pos+1);
    }

    public final static function getDependencyTag()
    {
        return static::getCacheKey('');
    }
}
