<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\widgets;

/**
 * Description of ActiveField
 *
 * @author Tartharia
 */
class ActiveField extends \yii\bootstrap\ActiveField
{
    public $autoPlaceholder = false;

    protected function createLayoutConfig($instanceConfig)
    {
        $config = parent::createLayoutConfig($instanceConfig);
        if($instanceConfig['autoPlaceholder'])
        {
            $config['inputOptions']['placeholder'] = $instanceConfig['model']->getAttributeLabel($instanceConfig['attribute']);
        }
        return $config;
    }

    public function dropDownList($items, $options = array())
    {
        if($this->autoPlaceholder)
        {
            $options['prompt'] = $this->model->getAttributeLabel($this->attribute);
        }
        return parent::dropDownList($items, $options);
    }
}
