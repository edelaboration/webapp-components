<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\helpers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Description of BootstrapLabelHelper
 *
 * @author Tartharia
 */
class BootstrapComponentHelper
{
    /**
     *
     * @param string $content
     * @param mixed $value
     * @param [] $types
     * @return string
     */
    public static function label($content,$value,$types=[])
    {
        $labelType = ArrayHelper::getValue($types, $value, 'default');
        return Html::tag('span',$content,['class'=>"label label-{$labelType}"]);
    }
}
