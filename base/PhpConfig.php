<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\base;

use Yii;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

/**
 * Description of PhpConfig
 *
 * @author Tartharia
 */
class PhpConfig implements ConfigAccess
{
    public $configFile;

    private $_params;

    private $_keys = [];

    public function get($key, $throwException = false)
    {
        $value = ArrayHelper::getValue(Yii::$app->params, $key ,$this->keyNotExist($key));

        if($value === $this->keyNotExist($key))
        {
            if($throwException)
            {
                throw new \yii\base\Exception("Parameter `{$key}` not exist in app config");
            }
            return null;
        }

        $unserialize = @unserialize($value);
        return ($unserialize !== false || $value == serialize(false)) ? $unserialize : $value;
    }

    public function set($key, $value, $serialize = false)
    {
        if($serialize)
        {
            $value = serialize($value);
        }
        $this->loadFromFile();
        ArrayHelper::setValue($this->_params, $key, $value);
        ArrayHelper::setValue(Yii::$app->params, $key, $value);
        $this->saveToFile();
    }

    public function remove($key)
    {
        $this->loadFromFile();
        $value = ArrayHelper::remove($this->_params, $key, $this->keyNotExist($key));
        if($value !== $this->keyNotExist($key))
        {
            ArrayHelper::remove(Yii::$app->params, $key);
            $this->saveToFile();
            return true;
        }else{
            return false;
        }
    }

    protected function loadFromFile()
    {
        $this->_params = require Yii::getAlias($this->configFile);
    }

    protected function saveToFile()
    {
        file_put_contents(Yii::getAlias($this->configFile),  "<?php\nreturn " . VarDumper::export($this->_params). ";\n",LOCK_EX);
        unset($this->_params);
    }

    private function keyNotExist($key)
    {
        if(!key_exists($key, $this->_keys))
        {
            $this->_keys[$key] = "key:{$key}not_exist" . Yii::$app->security->generateRandomString(4);
        }
        return $this->_keys[$key];
    }
}
