<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\base;

/**
 *
 * @author Tartharia
 */
interface ConfigAccess
{
    /**
     *
     * @param string $key
     * @return mixed
     */
    public function get($key, $throwException = false);

    /**
     *
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value, $serialize = false);

    /**
     *
     * @param string $key
     */
    public function remove($key);
}
