<?php

namespace robote13\yii2components\behaviors;

use Yii;
use yii\db\BaseActiveRecord;
use yii\validators\UniqueValidator;

/**
 * Description of IndexedVarcharBehavior
 *
 * @author Tartharia
 */
class IndexedStringBehavior extends \yii\base\Behavior{

    /**
     * @var string the attribute, that used to create the index
     */
    public $indexAttribute;

    /**
     * @var string
     */
    public $attribute;

    public function events() {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE=>'generateSlugIndex',
            BaseActiveRecord::EVENT_AFTER_VALIDATE=>'validateSlug',
        ];
    }

    public function generateSlugIndex($event) {
        if($this->owner{$this->attribute})
        {
            $this->owner->{$this->indexAttribute} = md5($this->owner{$this->attribute});
        }
    }

    public function validateSlug($event)
    {
        if($this->owner->hasErrors($this->attribute))
        {
            return;
        }
        $model = clone $this->owner;
        $model->clearErrors();
        $validator = new UniqueValidator([
            'message' => Yii::t('app','{attribute} "{value}" has already been taken.',['attribute'=> ucfirst($this->attribute),'value'=>$model->{$this->attribute}])
        ]);
        $validator->validateAttribute($model, $this->indexAttribute);
        if($model->hasErrors())
        {
            $this->owner->addError($this->attribute,$model->getFirstError($this->indexAttribute));
        }
    }

    /**
     *
     * @param string $param
     * @return \yii\db\ActiveQuery
     */
    public function findByIndexed($param)
    {
        /* @var $model \yii\db\ActiveRecord */
        $model = $this->owner;
        return $model->find()->where("{$this->indexAttribute}=:attr",['attr'=>  md5($param)]);
    }
}