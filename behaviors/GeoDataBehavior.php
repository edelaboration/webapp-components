<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\behaviors;

use yii\db\ActiveRecord;

/**
 * Description of GeoDataBehavior
 *
 * @property ActiveRecord $owner
 */
class GeoDataBehavior extends \yii\base\Behavior{

    public $relation;

    public $className;

    public function events() {
        return[
            ActiveRecord::EVENT_INIT => 'initHandler',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate'
        ];
    }

    public function initHandler()
    {
        $this->owner->populateRelation($this->relation, \Yii::createObject($this->className));
    }

    public function afterFind($event) {
        $getter = 'get'.ucfirst($this->relation);
        $related = $this->owner->{$getter}()->findFor($this->relation, $this->owner);
        if($related instanceof \yii\db\ActiveRecord)
        {
            $this->owner->populateRelation($this->relation, $related);
        }
    }

    public function afterValidate() {
        if(!$this->owner->{$this->relation}->validate())
        {
            $this->owner->addError($this->owner->activeAttributes()[0],'Геоданные не могут быть сохранены');
        }
    }

    public function afterInsert()
    {
        $this->owner->link($this->relation, $this->owner->{$this->relation});
    }

    public function afterUpdate()
    {
        $this->owner->{$this->relation}->save(false);
    }

    public function load()
    {
        throw new \yii\base\Exception(12);
    }
}
