<?php

/*
 * The MIT License
 *
 * Copyright 2019 Tartharia.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace robote13\yii2components\behaviors;

/**
 * Description of AntibotEmptyString
 *
 * @author Tartharia
 */
class AntibotEmptyString extends \yii\base\Behavior
{

    public $attribute;

    private $_attributeValue;

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true) {
        return $name === $this->attribute || parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function __get($name) {
        return $name === $this->attribute? $this->_attributeValue : parent::__get($name);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true) {
        return $name === $this->attribute || parent::canSetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value) {
        if($name === $this->attribute)
        {
            $this->_attributeValue = $value;
        }else{
            parent::__set($name, $value);
        }
    }
}
