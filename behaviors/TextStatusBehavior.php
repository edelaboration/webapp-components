<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\behaviors;

use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;

/**
 * Description of TextStatusBehavior
 *
 * @author Tartharia
 */
class TextStatusBehavior extends \yii\base\Behavior{

    public $attributes;


    public function init() {
        if (!isset($this->attributes))
        {
            throw new InvalidConfigException ('The "attributes" property will be set.');
        }
    }

    public function canGetProperty($name, $checkVars = true) {
        return $this->masterAttribute($name) !== null ? true : parent::canGetProperty($name, $checkVars);
    }

    public function __get($name) {

        $attribute = $this->masterAttribute($name);
        if($attribute !== null)
        {
            $statuses = ArrayHelper::getValue($this->attributes, $attribute,[]);
            $value = ArrayHelper::getValue($statuses, $this->owner->{$attribute}, "Unknown {$attribute}");
        }
        return isset($value)? $value: parent::__get($name);
    }

    /**
     *
     * @param string $slaveAttribute
     * @return string|null
     */
    public function masterAttribute($slaveAttribute)
    {
        $postfixPos = strrpos($slaveAttribute, 'Text');

        if(strlen($slaveAttribute) === $postfixPos + 4)
        {
            $attribute = substr($slaveAttribute,0,$postfixPos);
        }
        return isset($attribute) && $this->owner->hasProperty($attribute)? $attribute : null;
    }
}
