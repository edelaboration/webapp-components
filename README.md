Yii2 web application components
===============================

Different components for Yii2 web application

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist robote13/yii2-components "^1.1.0"
```

or add

```
"robote13/yii2-components": "^1.1.0"
```

to the require section of your `composer.json` file.


Upgrade to 1.0.0
-----

CrudControllerAbstract::$indexViewParams -> $indexParams (now the name of this property is the same for Crud and FrontendController)