<?php
/**
 * @package tours
 */

namespace robote13\yii2components\web;

use Yii;
use yii\filters\VerbFilter;

/**
 * CrudControllerAbstract implements the CRUD actions for ToursModule models.
 */
abstract class CrudControllerAbstract extends ControllerAbstract
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = Yii::createObject($this->modelClass);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect($this->getRedirectUri());
        }
            else
            {
                return $this->render('create', ['model' => $model]);
            }
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param mixed $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $id = Yii::$app->getRequest()->get('id');
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect($this->getRedirectUri());
        }
            else
            {
                return $this->render('update', ['model' => $model]);
            }
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $id = \Yii::$app->getRequest()->get('id');
        $this->findModel($id)->delete();

        return $this->redirect($this->getRedirectUri());
    }
}