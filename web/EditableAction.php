<?php

namespace robote13\yii2components\web;

use Yii;
use yii\web\Response;
use yii\helpers\ArrayHelper;


/**
 * Description of EditableAction
 *
 * @author Tartharia
 */
class EditableAction extends \yii\rest\Action
{
    public function run()
    {
        $this->validate();
        $model = $this->findModel(ArrayHelper::getValue(Yii::$app->request->post(),'editableKey'));
        if($model->load(Yii::$app->request->post()) && $model->save())
        {
           $result = ['output'=>'','message'=> ''];
        }else{
           $result = ['output'=>'','message'=>$model->getFirstError()];
        }
        return Yii::createObject(['class' => Response::className(), 'format' => Response::FORMAT_JSON, 'data' => $result]);
    }

    protected function validate()
    {
        if(!Yii::$app->request->isPost && !Yii::$app->request->isAjax)
        {
            throw new \yii\web\BadRequestHttpException('Operation not allowed');
        }
    }
}
