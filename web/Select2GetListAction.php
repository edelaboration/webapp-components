<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\web;

use Yii;
use yii\web\Response;
use yii\helpers\ArrayHelper;

/**
 * Description of Select2GetListAction
 *
 * @author Tartharia
 */
class Select2GetListAction extends \yii\base\Action
{
    public $modelClass;

    public $searchAttribute;

    public function run()
    {
        $modelClass = $this->modelClass;

        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->getRequest()->post();
        $q = ArrayHelper::getValue($post, 'q');
        $exclude = ArrayHelper::getValue($post, 'exc');
        $id = ArrayHelper::getValue($post, 'id');
        $out = ['results' => []];

        if ($q !== null) {
            $query = $modelClass::find();
            $query->andWhere(['like', $this->searchAttribute, $q])
                    ->andFilterWhere(['not',['id'=>$exclude]])
                    ->asArray();
            $out['results'] = $query->all();
        }
        elseif ($id > 0) {
            $out['results'] = [$modelClass::find()->where(['id'=>$id])->asArray()->one()];
        }
        return $out;
    }
}
