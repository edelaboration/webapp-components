<?php

namespace robote13\yii2components\web;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Description of Modifier
 *
 * @author Tartharia
 */
abstract class Modifier
{
    /**
     *
     * @param Model $searchModel
     * @param ActiveDataProvider $dataProvider
     * @return array
     */
    abstract public function modify(Model $searchModel, ActiveDataProvider $dataProvider);
}
