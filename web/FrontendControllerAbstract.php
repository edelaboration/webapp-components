<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\yii2components\web;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * FrontendControllerAbstract implements the typically frontend actions.
 *
 * @property-read string $modelClass
 * @property-read string $searchClass
 */
abstract class FrontendControllerAbstract extends ControllerAbstract
{

    /**
     * @var \Closure
     */
    protected $findModelCallback;

    /**
     *
     * @param string $id
     * @return \yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $modelClass = Yii::$container->has($this->modelClass)?$this->getDefinition($this->modelClass):$this->modelClass;

        $query = $modelClass::find();
        if($this->findModelCallback instanceof \Closure)
        {
            $query = call_user_func($this->findModelCallback, $query, $id);
        }else{
            $query->andWhere(['id'=>$id]);
        }
        if(!($model=$query->one()))
        {
            throw new NotFoundHttpException(Yii::t('app', 'Requested page not found'));
        }
        return $model;
    }
}
