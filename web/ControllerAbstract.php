<?php

namespace robote13\yii2components\web;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


/**
 * Description of ControllerAbstract
 *
 * @property null|Modifier $modifier модификатор параметров передающихся из actionIndex в {@see \yii\web\View::render($view, $params)}
 * @property-read string $modelClass
 * @property-read string $searchClass
 * @author Tartharia
 */
abstract class ControllerAbstract extends \yii\web\Controller
{
    /**
     * @deprecated since version 1.1.0
     * @var \Closure
     */
    protected $indexParams;

    /**
     * @var null|string|Modifier
     */
    private $_modifier;

    /**
     * @return string model class name
     */
    abstract public function getModelClass();

    /**
     * @return string search model class name
     */
    abstract public function getSearchClass();


    /**
     *
     * @return \Closure
     * @deprecated since version 1.1.0
     */
    protected function getIndexViewParams()
    {
        return $this->indexParams;
    }

    /**
     *
     * @param \Closure $callback
     * @deprecated since version 1.1.0
     */
    protected function setIndexViewParams(\Closure $callback)
    {
        $this->indexParams = $callback;
    }

    /**
     * Setter for indexAction modifier
     * @param string $className
     */
    public function setModifier($className)
    {
        $this->_modifier = $className;
    }

    public function getModifier()
    {
        if(is_string($this->_modifier))
        {
            $this->_modifier = \yii\di\Instance::ensure($this->_modifier, 'robote13\yii2components\web\Modifier');
        }

        return $this->_modifier;
    }

    /**
     * Lists all models.
     * When called, the current uri is stored in the session variable.
     * So that it was possible to return to the index action with the stored state of the filters and sorting
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember('', $this->getUriStorageKey());

        $searchModel = Yii::createObject($this->searchClass);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', $this->modify($searchModel, $dataProvider));
    }

    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', ['model' => $this->findModel($id)]);
    }

    /**
     * Вычисляет имя переменной сессии в которой хранится предыдущий uri для заданного экшена.
     * @see Url::remember
     * @see Url::previous
     * @param string $action
     * @return string key name
     */
    protected function getUriStorageKey($action = 'index')
    {
        return "{$this->module->id}-{$this->id}-{$action}";
    }

    /**
     *
     * @param string $action
     * @return string
     */
    protected function getRedirectUri($action = 'index')
    {
        $previous = Url::previous($this->getUriStorageKey($action));
        return !empty($previous)?$previous:['index'];
    }

    /**
     *
     * @param \yii\base\Model $searchModel
     * @param \yii\data\ActiveDataProvider $dataProvider
     * @return array
     */
    protected function modify($searchModel,$dataProvider)
    {
        if($this->modifier !== null)
        {
            return $this->modifier->modify($searchModel, $dataProvider);
        }
        if($this->indexParams instanceof \Closure)
        {
            $params = compact('searchModel','dataProvider');
            return ArrayHelper::merge($params,call_user_func($this->indexParams, $searchModel, $dataProvider));
        }
        return compact('searchModel','dataProvider');
    }

    /**
     *
     * @param string $className
     * @return string
     */
    protected final function getDefinition($className)
    {
        return Yii::$container->has($className)
            ?ArrayHelper::getValue(Yii::$container->getDefinitions()[$className],'class',$className)
            :$className;
    }

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \yii\db\ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $className = $this->getDefinition($this->modelClass);

        if (($model = $className::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
