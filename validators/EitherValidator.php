<?php

namespace robote13\yii2components\validators;

/**
 * EitherValidator validates that at least one attribute has been set.
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class EitherValidator extends \yii\validators\Validator
{
    public $strict = false;

    public $targetAttribute;

    public function init() {
        parent::init();
        if(!isset($this->message))
        {
            $this->message = \Yii::t('app', "Either must be set.");
        }
    }

    public function validateAttributes($model, $attributes = null)
    {
        if($this->when !== null && !call_user_func($this->when,$model))
        {
            return;
        }

        $validatingAttributes = $this->attributes;
        if(is_array($attributes))
        {
            foreach ($validatingAttributes as $key => $attribute)
            {
                if(!in_array($attribute, $attributes))
                {
                    unset($validatingAttributes[$key]);
                }
            }
        }

        $required = new \yii\validators\RequiredValidator(['strict' => $this->strict,'isEmpty'=>$this->isEmpty]);
        foreach ($validatingAttributes as $attribute) {
            if($required->validate($model->{$attribute}))
            {
                return;
            }
        }

        if(count($validatingAttributes) !== 0)
        {
            $this->addError($model, isset($this->targetAttribute)?$this->targetAttribute:$validatingAttributes[0], $this->message);
        }
    }

    /**
     *
     * @param \yii\db\ActiveRecord $model
     * @param type $attribute
     * @param type $view
     * @return type
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        if($this->targetAttribute && $this->targetAttribute != $attribute)
        {
            return null;
}
        $attributes=[];
        foreach ($this->attributes as $id) {
            $attributes[]= \yii\helpers\Html::getInputId($model, $id);
        }
        $inputIds = json_encode($attributes);
        $message = json_encode($this->message,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return <<<JS
var attributes = $inputIds, message = $message,count = attributes.length;
var empties = attributes.filter(function(id){
     return $('#'+id).val() === '';
})
if(empties.length == count)
{
    messages.push(message);
}
JS;
    }
}