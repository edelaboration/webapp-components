<?php

namespace robote13\yii2components\validators;

use Yii;

/**
 * Description of EmptyStringValidator
 *
 * @author Tartharia
 */
class EmptyStringValidator extends \yii\validators\RequiredValidator
{
    /**
     * @var mixed the desired value that the attribute must have.
     * If this is null, the validator will validate that the specified attribute is not empty.
     * If this is set as a value that is not null, the validator will validate that
     * the attribute has a value that is the same as this property value.
     * Defaults to null.
     * @see strict
     */
    public $requiredValue = '';
    /**
     * @var bool whether the comparison between the attribute value and [[requiredValue]] is strict.
     * When this is true, both the values and types must match.
     * Defaults to false, meaning only the values need to match.
     *
     * Note that behavior for when [[requiredValue]] is null is the following:
     *
     * - In strict mode, the validator will check if the attribute value is null
     * - In non-strict mode validation will fail
     */
    public $strict = true;

    public function init() {
        if($this->message === null){
            $this->message = Yii::t('yii', '{attribute} is invalid.');
        }
        parent::init();
    }
}
